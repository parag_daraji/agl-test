﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGL.Service.UnitTest
{
    [TestClass]
    public class PeopleServiceTest
    {
        [TestMethod]
        public void PeopleService_return_Null_with_wrong_AppSetting_URL()
        {
            //// Arrange
            var peopleService = new PeopleService("test");

            //// Act
            var peopleList = peopleService.GetAllPeople();

            //// Assert

            Assert.IsNull(peopleList);
        }

        [TestMethod]
        public void PeopleService_return_All_people_with_No_AppSetting_URL()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act
            var peopleList = peopleService.GetAllPeople();

            //// Assert

            Assert.IsNotNull(peopleList);
        }

        [TestMethod]
        public void PeopleService_return_All_people_with_Count()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act
            var peopleList = peopleService.GetAllPeople();

            //// Assert

            Assert.AreEqual(peopleList.Count(), 6);
        }

        [TestMethod]
        public void PeopleService_GetPersonByGender_return_Null_With_Empty_Gender_parameter()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act
            var peopleList = peopleService.GetPersonByGender(string.Empty);

            //// Assert

            Assert.IsNull(peopleList);
        }

        [TestMethod]
        public void PeopleService_GetPersonByGender_return_Male_person_Only()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act
            var allPeopleList = peopleService.GetAllPeople();
            var malePeopleList = peopleService.GetPersonByGender("Male");

            var maleCountInList = allPeopleList.Count(src => src.Gender.Equals("Male"));

            //// Assert

            Assert.AreEqual(malePeopleList.Count(), maleCountInList);
        }

        [TestMethod]
        public void PeopleService_GetPersonByGender_return_Female_person_Only()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act
            var allPeopleList = peopleService.GetAllPeople();
            var femalePeopleList = peopleService.GetPersonByGender("Female");

            var femaleCountInList = allPeopleList.Count(src => src.Gender.Equals("Female"));

            //// Assert

            Assert.AreEqual(femalePeopleList.Count(), femaleCountInList);
        }


        [TestMethod]
        public void PeopleService_GetCatsByPersonGender_return_Cats_For_Male()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act

            var catPetsForMale = peopleService.GetCatsByPersonGender("male");

            //// Assert
            Assert.AreEqual(catPetsForMale.Count(), 4);
        }

        [TestMethod]
        public void PeopleService_GetCatsByPersonGender_return_Cats_For_Female()
        {
            //// Arrange
            var peopleService = new PeopleService();

            //// Act

            var catPetsForFemale = peopleService.GetCatsByPersonGender("female");

            //// Assert
            Assert.AreEqual(catPetsForFemale.Count(), 3);

        }
    }
}
