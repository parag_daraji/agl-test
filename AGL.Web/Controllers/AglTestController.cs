﻿using AGL.Service;
using AGL.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGL.Web.Controllers
{
    public class AGLTestController : Controller
    {
        private readonly IPeopleService _peopleService = null;

        public AGLTestController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        //
        // GET: /AGLTest/
        public ActionResult Index()
        {
            var viewModel = this.InitializeViewModel();
            return View(viewModel);
        }


        private AglTestViewModel InitializeViewModel()
        {
            var viewModel = new AglTestViewModel();
            viewModel.MaleGenderTitle = "Male"; // Can get from CMS
            viewModel.CatsOwnByMale = this._peopleService.GetCatsByPersonGender("male");

            viewModel.FemaleGenderTitle = "Female"; // Can get from CMS
            viewModel.CatsOwnByFemale = this._peopleService.GetCatsByPersonGender("female");
            return viewModel;
        }
    }
}
