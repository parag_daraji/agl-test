﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AGL.Web.Startup))]
namespace AGL.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
