﻿using AGL.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGL.Web.Models
{
    public class AglTestViewModel
    {
        public string MaleGenderTitle { get; set; }

        public IEnumerable<IPet> CatsOwnByMale { get; set; }

        public string FemaleGenderTitle { get; set; }

        public IEnumerable<IPet> CatsOwnByFemale { get; set; }
    }
}