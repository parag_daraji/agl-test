# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for AGL sample test
* Version - 1.0

### How do I get set up? ###

* After clone, open AGL.Test.sln
* Build the whole solution, this will download nuget packages. This has been removed to save space.
* Set AGL.Web as start project
* Hit F5 key for starting execution
* On the home page of standard ASP.Net MVC application, click on "AGL test" menu option in the header navigation


Repo owner or admin - Parag Daraji