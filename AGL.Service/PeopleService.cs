﻿using AGL.Service.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

namespace AGL.Service
{
    public class PeopleService : IPeopleService
    {
        #region Private variables

        private string jsonString;

        #endregion

        #region Concreate implementation

        public PeopleService(string appsettingName = "Person.Service.URL")
        {
            var jsonServiceUrl = ConfigurationManager.AppSettings[appsettingName];
            if (string.IsNullOrWhiteSpace(jsonServiceUrl))
            {
                jsonString = string.Empty;
                return;
            }

            var request = WebRequest.Create(jsonServiceUrl);
            request.ContentType = "application/json; charset=utf-8";

            var response = (HttpWebResponse)request.GetResponse();

            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                jsonString = sr.ReadToEnd();
            }

            //// manually disposing objects
            request = null;
            response = null;
        }

        public IEnumerable<Models.IPerson> GetAllPeople()
        {
            if (string.IsNullOrWhiteSpace(jsonString))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<List<Person>>(jsonString);
        }

        public IEnumerable<Models.IPerson> GetPersonByGender(string gender)
        {
            if (string.IsNullOrWhiteSpace(gender))
            {
                return null;
            }

            var peopleList = this.GetAllPeople().ToList();
            if (peopleList == null || !peopleList.Any())
            {
                return null;
            }

            return peopleList.Where(p => p.Gender.Equals(gender, StringComparison.InvariantCultureIgnoreCase));
        }

        public IEnumerable<Models.IPet> GetCatsByPersonGender(string gender)
        {
            if (string.IsNullOrWhiteSpace(gender))
            {
                return null;
            }

            var personListByGender = this.GetPersonByGender(gender);
            if (personListByGender == null || !personListByGender.Any())
            {
                return null;
            }

            return personListByGender.Where(p => p.Pets != null && p.Pets.Any(p1 => p1.Type.Equals("cat", StringComparison.InvariantCultureIgnoreCase)))
            .SelectMany(src => src.Pets.Where(p => p != null && p.Type.Equals("cat", StringComparison.InvariantCultureIgnoreCase)))
            .OrderBy(o => o.Name);
        }

        #endregion
    }
}