﻿using AGL.Service.Models;
using System.Collections.Generic;

namespace AGL.Service
{
    public interface IPeopleService
    {
        IEnumerable<IPerson> GetAllPeople();

        IEnumerable<IPerson> GetPersonByGender(string gender);

        IEnumerable<IPet> GetCatsByPersonGender(string gender);
    }
}
