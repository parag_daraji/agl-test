﻿
namespace AGL.Service.Models
{
    public interface IPet
    {
        string Name { get; set; }

        string Type { get; set; }
    }
}
