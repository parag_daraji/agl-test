﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace AGL.Service.Models
{
    public interface IPerson
    {
        string Name { get; set; }

        string Gender { get; set; }

        int Age { get; set; }

        IList<Pet> Pets { get; set; }
    }
}
