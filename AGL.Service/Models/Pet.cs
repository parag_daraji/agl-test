﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Service.Models
{
    public class Pet : IPet
    {
        public string Name { get; set; }

        public string Type { get; set; }
    }
}
