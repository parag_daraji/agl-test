﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Service.Models
{
    public class Person : IPerson
    {
        public string Name { get; set; }

        public string Gender { get; set; }

        public int Age { get; set; }

        [JsonProperty(TypeNameHandling = TypeNameHandling.Objects)]
        public IList<Pet> Pets { get; set; }
    }
}
